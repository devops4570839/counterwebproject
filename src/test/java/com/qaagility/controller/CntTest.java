package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CntTest {

    @Test
    public void testD() {
        Cnt cnt = new Cnt();
        int expected = 2;
        assertEquals(expected, cnt.d(4, 2));
    }

    @Test
    public void testDWithZero() {
        Cnt cnt = new Cnt();
        int expected = Integer.MAX_VALUE;
        assertEquals(expected, cnt.d(4, 0));
    }
}

