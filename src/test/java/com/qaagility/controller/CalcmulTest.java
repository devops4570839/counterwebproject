package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CalcmulTest {

    @Test
    public void testMul() {
        Calcmul calcmul = new Calcmul();
        int expected = 18;
        assertEquals(expected, calcmul.mul());
    }
}

